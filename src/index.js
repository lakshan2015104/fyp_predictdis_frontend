    
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import './App.css'
import App from './App';
import Result from './Result';

const routing = (
    <Router>
      <div>
        <Route path="/index" component={App} />
        <Route path="/result" component={Result} />
      </div>
    </Router>
  )

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(routing, document.getElementById('root'))