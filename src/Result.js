import React, { Component } from 'react';
import './App.css';


class Result extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedFile: '',
            dynamicHtml: '',
            imageType: '',
            passData: null
        }
        this.onValueChange = this.onValueChange.bind(this);
        this.instance = null;
    }

    componentDidMount() {
        const { location } = this.props;
        if (location.state.passData) {
            const { html, file, type } = location.state.passData;
            console.log(file);
            this.setState({ dynamicHtml: html, selectedFile: file, imageType: type })
        }


        //this.setState({selectedFile: location.state.selectedFile})
        //this.setState({imageType: location.state.imageType})
    }

    onValueChange = event => {
        this.setState({
            dynamicHtml: event.target.value
        });
    };


    render() {
        const { dynamicHtml, selectedFile, imageType } = this.state;

        if (this.instance) {
            setTimeout(() => this.instance.refresh(), 200);
        }

        return (
            <div className="container-fluid h-100">

                <h2 className="aligncenter">Results of the input</h2>

                <div className="row justify-content-center h-100">

                    <div className="col-md-4">
                        <img className="customImage" src={`data:${imageType};base64,${selectedFile}`} />
                    </div>

                    <div className="col-md-4">
                        <div className="custom-textarea"
                        value={dynamicHtml} onChange={this.onValueChange} wrap="hard" spellCheck="false">{dynamicHtml}
                    </div>

            
                    </div>

                </div>
                <a href='http://localhost:3000/index' class="btn process-btn" onClick={this.fileUploadHandler} ><i class="far fa-hand-point-right"></i>Go Back</a> 
            </div>
        );
    }

}

export default Result;