import React, { Component } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import PropTypes from 'prop-types';
import { createHashHistory } from 'history';
import './App.css';
import { debug } from 'util'



//const history = createHashHistory();
const endpoint = process.env.SKETCH_TO_CODE_API_ENDPOINT


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedFile:null,
      html: '',
      imageType: '',
      loading: false
    }
  }

  fileSelectedHandler = event => {
    let self = this;
    const promise = new Promise((resolve, reject) => {

      self.setState({ imageType: event.target.files[0].type });
      
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        if (reader.result) {
          resolve(reader.result);
        } else {
          reject(Error('Failed converting to base64'));
        }
      };
    });
    promise.then((result) => {
      let converted = result.split(',')[1]
       this.setState({ selectedFile: converted}, () => {
        console.log('State updated')
        
      })
      console.log(converted)
    }, (err) => {
      console.log(err);
    });
  }

 
  fileUploadHandler =() =>{
    //this.props.history.push('./Editor')
    this.onShow();
    let body = {
      "image": this.state.selectedFile
    }
    let self = this;
    axios.post('http://localhost:5000/upload', body)
    .then(function (response) {
      self.setState({ html: response.data.data }, () => {
        //console.log("Response: ", response.data.data);
        debugger
        console.log('FILE', self.state.selectedFile)
        let obj = {
          html: self.state.html,
          file: self.state.selectedFile,
          type: self.state.imageType
        };
        self.props.history.push({pathname: './result', state: { passData: obj }})
        // self.props.history.push({pathname: './editor', state: { dynamicHtml: self.state.html }})
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  clearFile =() =>{
    debugger
    this.setState({ selectedFile: null});
    
  }

  onShow = ()=> {
    this.setState({ loading: true })
  }

  onHide = ()=> {
    this.setState({ loading: false })
  }
  
  render() {
    const image = this.state.selectedFile;
    let fileExist = false;
    if(image) {
      fileExist = true;
    }
    return (
    <div className="App">
        {/* <input type="file" onChange={this.fileSelectedHandler}/>
       */}
  

    <script className="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    
    <div className="navbar">
        <img src="logo.png" className="logo"/>
        <h3>Nerve Damage Prediction System</h3>
     </div>

    <div class="outer-area">
     <div class="title">
        <h1>Nerve Damage Prediction System</h1>
      </div>


    <div className="file-upload">

      {
          fileExist ? (
  
               <div class="file-upload-content">
                <img class="file-upload-image" src={`data:${this.state.imageType};base64,${this.state.selectedFile}`} alt="your image" />
                <div class="image-title-wrap">
                  <button type="button" onClick= {this.clearFile} class="remove-image">Remove <span class="image-title"></span></button>
                </div>
              </div>
            ): (
          
               <div class="image-upload-wrap">
                 <label class="custom-file-upload">
                  <input class="file-upload-input" type='file' onChange={this.fileSelectedHandler} accept="image/*"/>Browse for a image graph report
                </label>
              </div>
            )
      }
    </div>
    <button class="btn process-btn" onClick={this.fileUploadHandler} ><i class="far fa-hand-point-right"></i>Process</button> 
   
    </div>
    </div>
    
    );
  }
}



export default App;